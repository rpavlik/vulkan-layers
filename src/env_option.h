// Copyright 2019-2022, Collabora, Ltd.
// SPDX-License-Identifier: BSL-1.0
/*!
 * @file
 * @brief  Environmental helpers.
 * @author Jakob Bornecrantz <jakob@collabora.com>
 * @ingroup aux_util
 *
 * Copied from Monado.
 * Debug get option helpers heavily inspired from mesa ones.
 */

#pragma once

#include <stdbool.h>


#ifdef __cplusplus
extern "C" {
#endif


bool
debug_get_bool_option(const char *name, bool _default);


#define DEBUG_GET_ONCE_BOOL_OPTION(suffix, name, _default)                                                             \
	static bool debug_get_bool_option_##suffix()                                                                   \
	{                                                                                                              \
		static bool gotten = false;                                                                            \
		static bool stored;                                                                                    \
		if (!gotten) {                                                                                         \
			gotten = true;                                                                                 \
			stored = debug_get_bool_option(name, _default);                                                \
		}                                                                                                      \
		return stored;                                                                                         \
	}

#ifdef __cplusplus
}
#endif
